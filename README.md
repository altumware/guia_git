# Guía Rapida de GIT

## Intro
Git es un sistema distribuido de control de versiones. El software es gratuito y open source y debe ser descargado a la computadora local desde [https://git-scm.com/](https://git-scm.com/). Git está diseñado para manejar desde pequeños hasta grandes proyectos con velocidad y eficiencia.


## 1. Configuración de un usuario Global
Este paso se realiza solo la primera vez que se utiliza Git. Desde la consola del sistema operativo debemos ejecutar:  
`git config --global user.name "nombre_usuario"`  
`git config --global user.email "email_usuario"`  


Ejemplo:  
`git config --global user.name "oscar.martinez.altum"`  
`git config --global user.email "oscar.martinez@altumware.com"`  


Para revisar o confirmar la configuración global ejecute:  
`git config --global -l`

## 2. Clonar un repositorio: git clone
Si queremos descargar el código desde un repositorio Web, por ejemplo si borramos nuestro proyecto y queremos volver a descargarlo o queremos instalar nuestro proyecto en otra maquina solo tenemos que copiar la url de nuestro proyecto, para ello pulsamos en clone or download y copiamos la url y ejecutamos el siguiente comando:  
`git clone url.git`

Ejemplo:  
`git clone https://gitlab.com/altumware/guia_git.git`

Nota: Esta instrucción clonará o descargará el proyecto en la carpeta en la que este posicionada la consola en ese momento.

## 3. Revisar estado de tus modificaciones
Para conocer el estado de nuestros archivos utilizamos el siguiente comando:  
`git status`

Si no hay cambios por aplicar mostrará una respuesta como la siguiente:
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean

Si, por ejemplo, agregamos una carpeta con nuestro nombre de usuario y un archivo llamado hola.txt que contenga el texto “texto” dentro de él, mostrará:  
On branch master
Your branch is up to date with 'origin/master'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
        oscar.martinez.altum/

nothing added to commit but untracked files present (use "git add" to track)

## 4. Añadir archivos al control de versiones (stage): git add
Git tiene dos procesos para gestionar nuestros archivos. Primero los tenemos que añadir a una zona intermedia conocida como stage y después tenemos que confirmar el paso definitivo de los archivos al repositorio, en otras palabras, el commit de los cambios.

Para añadir los archivos de nuestro proyecto utilizamos el siguiente comando:  
`git add .`
El . indica que se agregarán todos los nuevos archivos contenidos en la carpeta administrada por git.

Con `git add .` pasaríamos todos los archivos al stage, pero si, por ejemplo, solo queremos pasar los cambios que hemos hecho en holamundo.html, utilizaremos el siguiente comando:  
`git add holamundo.html`

Para agregar una carpeta pordemo utilizar el comando:  
`git add nombre_carpeta/`

Si solo quieres confirmar los archivos que tengan una cierta extensión por ejemplo todos los .css podemos utilizar el asterisco  “*” como comodín de la siguiente manera:   
`git add *.css`

Para confirmar que se agregaron los cambios correctamente al stage, ejecutemos de nuevo el comando:  
`git status`
Se mostrarán los nuevos archivos o cambios a gestionar.

## 5. Confirmar los cambios en local: git commit
Para confirmar los cambios y que estos que pasen al repositorio final (repositorio local) utilizamos en siguiente comando:  
`git commit -m "Comentario"`

El parámetro -m sirve para indicar una descripción del commit que vamos a hacer. Se solicita que esta descripción sea lo más amplia posible.

### 5.1 Ver los cambios realizados: git log
Para ver un resumen las operaciones que hemos realizado utilizamos el siguiente comando:  
`git log --oneline`

### 5.2 Rectificar commit: git commit –amend
Si nos hemos equivocado al escribir el mensaje al hacer el commit podemos corregir utilizando el parametro –amend de la siguiente manera:  
`git commit --amend -m "Nuevo mensaje corregido para el commit"`

## 6. Actualizar los cambios del repositorio local al repositorio remoto – push
Para subir los cambios de nuestro proyecto utilizamos el siguiente comando:  
`git push -u origin master`  
Al ejecutar este comando nos pedirá que introduzcamos el usuario o el correo electrónico de nuestro usuario en GitHub o GitLab o el repositorio remoto que estemos utilizando.

## 7. Actualizar los cambios del repositorio remoto a nuestro repositorio local – fetch y pull
Cuando se han realizado cambios en el repositorio remoto, por ejemplo, que otro usuario ha añadido cambios y queremos actualizar estos cambios en nuestro repositorio local. Debemos usar git fetch origin, para bajar los cambios del repositorio remoto a la rama origin/master de nuestro repositorio local:  
`git fetch origin`  
Este comando deja los cambios en una rama temporal (lo podemos ver con `git status` o `git diff origin/master`) y para confirmar la aplicación de los cambios en nuestra rama local debemos ejecutar el siguiente comando:  
`git merge origin/master`  
Con esto ya tenemos los cambios remotos en nuestro repositorio local.

## 7.1 Comando Pull (fetch + merge)
Al usar el comando git pull estamos combinando git fetch + git merge. Con git pull nos ahorramos el usar un comando más, pero no conviene utilizar git pull si no estamos seguros de que cambios se pueda traer del repositorio remoto.  
`git pull origin master`

## Comandos adicionales
## 8. Volver a un estado anterior: git restore
Si deseamos regresar a la ultima versión confirmada de un archivo, solo debemos ejecutar el siguiente comando para descartar cualquier cambio posterior.  
`git restore file.ext`  
Donde file es el nombre del archivo y ext su extensión.

## 9. Regresar a una versión anterior o volver a un commit anterior: git checkout id_commit
Una de las grandes ventajas de utilizar git es que podemos volver a un estado anterior. Imaginemos una situación terrorífica pero bastante común, donde hemos realizado cambios en nuestro proyecto y nos da un error que hace que nada funcione, no conseguimos saber qué es lo que falla y queremos volver al estado anterior donde todo funcionaba. Con el comando git checkout podemos volver a un estado anterior. El comando es:  
`git checkout Commit_ID`
El Commit id es el indicado en la 1er columna y ejecutar por ejemplo:
Para obtener el Id del commit podemos ejecutar:  
`git log –oneline`  
`git checkout 5f4b299`

## 10. Mostrar diferencias entre los últimos cambios del archivo sin confirmar y la última versión del archivo que hicimos commit: git diff
Modifiquemos cualquier archivo dentro del directorio git. Ahora si ejecutamos el comando git status veremos que nos indica que se ha modificado ese archivo.Si ejecutamos   
`git diff`  
Nos muestra la diferencia del archivo actual con el contenido de ese archivo en el último commit que hemos realizado.

## 11. Volver a un estado anterior: git reset
Si ejecutamos `git log` vemos que después de cada commit viene un id o identificativo con una serie de números y letras, algo parecido a esto:  
commit 27b6e605decfd194e4f4ec627381156e9bbadf27  
Este código nos sirve para identificar el commit.
Con `git log --oneline` vemos los comits de una forma simplificada, el código es más corto pero también nos ayuda para hacer un reset y podemos utilizar cualquiera de las dos formas.

Si por ejemplo hemos eliminado un archivo y queremos volver al punto anterior a eliminarlo buscamos en el log el código identificador del commit y utilizamos `git reset --soft codigo_comit` o `git reset --hard codigo_commit`
Con --soft no recuperamos el archivo, solamente se sitúa en el stage en el momento en que eliminamos el archivo.
Con –hard volvemos al punto anterior a eliminar el archivo y el archivo vuelve a aparecer en el lugar en el que estaba antes de ser eliminado. Ejemplo:  
`git reset --hard 6cf7d1a76c51a2875e14d19ce1ecb78017b4fd94`

## 12. Visualizar estatus de repositorio local vs repositorio remoto
La instrucción `git remote update` y posterior `git status` permite visualizar cuantos commits te faltan por descargar del repositorio remoto.


## Temas adicionales por documentar:
* Gitignore
* Git Branch
* Git Merge
* Git Tags
